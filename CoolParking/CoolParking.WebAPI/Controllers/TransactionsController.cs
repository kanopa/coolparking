﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }
        [HttpGet("last")]
        public ActionResult GetLastTransaction()
        {
            try
            {
                return Ok(parkingService.GetLastParkingTransactions());

            }
            catch (Exception e)
            {
                return NotFound();
            }
        }
        [HttpGet("all")]
        public ActionResult GetAllTransaction()
        {
            try
            {
                return Ok(parkingService.ReadFromLog());
            }
            catch(Exception e)
            {
                return NotFound();
            }
        }
        [HttpPut("topUpVehicle")]
        public ActionResult GettopUpVehicle([FromBody] string vehicleId, decimal sum)
        {
            parkingService.TopUpVehicle(vehicleId, sum);
            return Ok();
        }
    }
}