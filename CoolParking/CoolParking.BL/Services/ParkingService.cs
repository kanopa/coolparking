﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public Settings settingsParking { get; set; }
        public List<TransactionInfo> transactions { get; set; }
        public ILogService LogService { get; set; }
        public ITimerService timerService { get; set; }
        public ITimerService logTimer { get; set; }
        public ParkingService(ITimerService timerService, ITimerService logTimer, ILogService LogService)
        {
            settingsParking = new Settings();
            transactions = new List<TransactionInfo>();
            this.LogService = LogService;
            this.timerService = timerService;
            this.logTimer = logTimer;
            this.timerService.Interval = settingsParking.Payment;
            this.timerService.Elapsed += WithdrawMoneyEvent;
            this.timerService.Stop();
            this.logTimer.Interval = settingsParking.RecordLog;
            this.logTimer.Elapsed += LogMoneyEvent;
            this.logTimer.Stop();
        }

        private void WithdrawMoneyEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            
            foreach (var item in settingsParking.Parking.vehicles)
            {
                if (item.VehicleType == VehicleType.Bus)
                {
                    if (item.Balance < settingsParking.RateBus && item.Balance > 0)
                    {
                        item.Balance -= decimal.Multiply(settingsParking.RateBus - item.Balance, settingsParking.Penalty);
                        settingsParking.Parking.Balance += decimal.Multiply(settingsParking.RateBus - item.Balance, settingsParking.Penalty);
                    }
                    else if (item.Balance < settingsParking.RateBus)
                    {
                        item.Balance -= decimal.Multiply(settingsParking.RateBus, settingsParking.Penalty);
                        settingsParking.Parking.Balance += decimal.Multiply(settingsParking.RateBus, settingsParking.Penalty);
                    }
                    else
                    {
                        item.Balance -= settingsParking.RateBus;
                        settingsParking.Parking.Balance += settingsParking.RateBus;
                    }
                    transactions.Add(new TransactionInfo(DateTime.UtcNow, item.Id, item.Balance));
                }
                else if (item.VehicleType == VehicleType.Motorcycle)
                {
                    if (item.Balance < settingsParking.RateMotorcycle && item.Balance > 0)
                    {
                        item.Balance -= decimal.Multiply(settingsParking.RateMotorcycle - item.Balance, settingsParking.Penalty);
                        settingsParking.Parking.Balance += decimal.Multiply(settingsParking.RateMotorcycle - item.Balance, settingsParking.Penalty);
                    }
                    else if (item.Balance < settingsParking.RateMotorcycle)
                    {
                        item.Balance -= decimal.Multiply(settingsParking.RateMotorcycle, settingsParking.Penalty);
                        settingsParking.Parking.Balance += decimal.Multiply(settingsParking.RateMotorcycle, settingsParking.Penalty);
                    }
                    else
                    {
                        item.Balance -= settingsParking.RateMotorcycle;
                        settingsParking.Parking.Balance += settingsParking.RateMotorcycle;
                    }
                    transactions.Add(new TransactionInfo(DateTime.UtcNow, item.Id, item.Balance));
                }
                else if (item.VehicleType == VehicleType.PassengerCar)
                {
                    if (item.Balance < settingsParking.RateCar && item.Balance > 0)
                    {
                        item.Balance -= decimal.Multiply(settingsParking.RateCar - item.Balance, settingsParking.Penalty);
                        settingsParking.Parking.Balance += decimal.Multiply(settingsParking.RateCar - item.Balance, settingsParking.Penalty);
                    }
                    else if (item.Balance < settingsParking.RateCar)
                    {
                        item.Balance -= decimal.Multiply(settingsParking.RateCar, settingsParking.Penalty);
                        settingsParking.Parking.Balance += decimal.Multiply(settingsParking.RateCar, settingsParking.Penalty);
                    }
                    else
                    {
                        item.Balance -= settingsParking.RateCar;
                        settingsParking.Parking.Balance += settingsParking.RateCar;
                    }
                    transactions.Add(new TransactionInfo(DateTime.UtcNow, item.Id, item.Balance));
                }
                else if (item.VehicleType == VehicleType.Truck)
                {
                    if (item.Balance < settingsParking.RateTruck && item.Balance > 0)
                    {
                        item.Balance -= decimal.Multiply(settingsParking.RateTruck - item.Balance, settingsParking.Penalty);
                        settingsParking.Parking.Balance += decimal.Multiply(settingsParking.RateTruck - item.Balance, settingsParking.Penalty);
                    }
                    else if (item.Balance < settingsParking.RateTruck)
                    {
                        item.Balance -= decimal.Multiply(settingsParking.RateTruck, settingsParking.Penalty);
                        settingsParking.Parking.Balance += decimal.Multiply(settingsParking.RateTruck, settingsParking.Penalty);
                    }
                    else
                    {
                        item.Balance -= settingsParking.RateTruck;
                        settingsParking.Parking.Balance += settingsParking.RateTruck;
                    }
                    transactions.Add(new TransactionInfo(DateTime.UtcNow, item.Id, item.Balance));
                }
            }
        }

        private void LogMoneyEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            foreach (var item in transactions)
            {
                LogService.Write(string.Format("Add new record, Id: {0}, Sum: {1}", item.IdVehicle, item.sum));
            }
        }

        public void AddVehicle(Vehicle vehicle)
        {
            ValidId(vehicle.Id);
            ValidBalance(vehicle.Balance);
            settingsParking.Parking.vehicles.Add(vehicle);
            timerService.Start();
            logTimer.Start();
        }

        public void ValidId(string id)
        {
            int res;
            foreach (var item in settingsParking.Parking.vehicles)
            {
                if(item.Id == id)
                    throw new ArgumentException();
            }
            if (id.ToUpper() != id)
            {
                throw new ArgumentException();
            }
            string[] temp = id.Split("-");
            if (temp.Length > 3 || temp.Length < 3)
            {
                throw new ArgumentException();
            }
            bool check = Int32.TryParse(temp[0], out res);
            if (check == false)
            {
                check = Int32.TryParse(temp[1], out res);
                if (check == true)
                {
                    check = Int32.TryParse(temp[2], out res);
                    if (check == false)
                    {
                        return;
                    }
                }
            }
            throw new ArgumentException();
        }

        public VehicleType ValidType(string type)
        {
            if (type == VehicleType.Bus.ToString())
                return VehicleType.Bus;
            else if (type == VehicleType.Motorcycle.ToString())
                return VehicleType.Motorcycle;
            else if (type == VehicleType.PassengerCar.ToString())
                return VehicleType.PassengerCar;
            else if (type == VehicleType.Truck.ToString())
                return VehicleType.Truck;

            else
            {
                throw new ArgumentException();
            }
        }

        public void ValidBalance(decimal balance)
        {
            if (balance < 0)
                throw new ArgumentException();
        }

        public void Dispose()
        {
        }

        public decimal GetBalance()
        {
            return settingsParking.Parking.Balance;
        }

        public int GetCapacity()
        {
            return settingsParking.Capacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - settingsParking.Parking.vehicles.Count;
        }

        public TransactionInfo GetLastParkingTransactions()
        {
            if(transactions.Count > 0)
            {
                return transactions[transactions.Count - 1];
            } else
            {
                throw new Exception();
            }
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehicles =
                new ReadOnlyCollection<Vehicle>(settingsParking.Parking.vehicles);

            return vehicles;
        }

        public Vehicle GetVehiclesById(string id)
        {
            ValidId(id);
            var temp = settingsParking.Parking.vehicles.Find(x => x.Id == id);
            if (temp == null)
            {
                throw new ArgumentNullException();
            }
            return temp;
        }

        public string ReadFromLog()
        {
            return LogService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var temp = settingsParking.Parking.vehicles.Find(x => x.Id == vehicleId);
            if (temp == null)
            {
                throw new ArgumentException();
            }
            settingsParking.Parking.vehicles.Remove(temp);
            if(settingsParking.Parking.vehicles.Count == 0)
            {
                timerService.Stop();
                logTimer.Stop();
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if(sum < 0)
                throw new ArgumentException();
            foreach (var item in settingsParking.Parking.vehicles)
            {
                if (item.Id != vehicleId)
                    throw new ArgumentException();
            }
            var temp = settingsParking.Parking.vehicles.Find(x => x.Id == vehicleId);
            temp.Balance += sum;
        }
    }
}
