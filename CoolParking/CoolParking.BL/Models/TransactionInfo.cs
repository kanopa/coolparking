﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime TimeTransaction { get; set; }
        public string IdVehicle { get; set; }
        public decimal sum { get; set; }

        public TransactionInfo(DateTime TimeTransaction, string IdVehicle, decimal Sum)
        {
            this.TimeTransaction = TimeTransaction;
            this.IdVehicle = IdVehicle;
            this.sum = Sum;
        }
        public decimal Sum(Func<TransactionInfo, decimal> func) {
            return func(this);
        }
    }
}