﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking
{
    class Program
    {
        static void Main(string[] args)
        {
            bool flag = true;
            string inputKey;
            List<Vehicle> vehicles = new List<Vehicle>();
            var logService = new LogService(@"C:\Users\Дима\Desktop\coolparking\log.txt");
            ITimerService timerService = new TimerService();
            ITimerService logTimer = new TimerService();
            var parkingService = new ParkingService(timerService, logTimer, logService);
            while (flag)
            {
                Console.Clear();
                Console.WriteLine("1. Create vehicle");
                Console.WriteLine("2. Output all non parked vehicles");
                Console.WriteLine("3. To the parking");
                Console.WriteLine("4. Exit");
                inputKey = Console.ReadLine();
                Console.Clear();
                switch (inputKey)
                {
                    case "1":
                        try
                        {
                            Console.WriteLine("Input your id vehicle");
                            string validId = Console.ReadLine();
                            parkingService.ValidId(validId);
                            Console.WriteLine("Input your balance");
                            decimal balance = Convert.ToDecimal(Console.ReadLine());
                            parkingService.ValidBalance(balance);
                            Console.WriteLine("Input your type vehicle");
                            VehicleType typeVehicle = parkingService.ValidType(Console.ReadLine());
                            Vehicle vehicle = new Vehicle(validId, typeVehicle, balance);
                            vehicles.Add(vehicle);
                        }
                        catch (ArgumentException e)
                        {
                            Console.WriteLine(e);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                        break;
                    case "2":
                        try
                        {
                            AllVehicles(vehicles);
                            Console.ReadKey();
                        }
                        catch (ArgumentException e)
                        {
                            Console.WriteLine(e);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                        break;
                    case "3":
                        inputKey = "";
                        bool flagInner = true;
                        while (flagInner)
                        {
                            Console.Clear();
                            Console.WriteLine("Welcome to Cool Parking \n\n");
                            Console.WriteLine("1. Park the car");
                            Console.WriteLine("2. Pick up a vehicle from parking");
                            Console.WriteLine("3. Display parked cars");
                            Console.WriteLine("4. Display current parking balance");
                            Console.WriteLine("5. Display the number of available / occupied parking spaces");
                            Console.WriteLine("6. Top up the balance of a specific vehicle");
                            Console.WriteLine("7. Display Last Parking Transactions for the current period");
                            Console.WriteLine("8. Print transaction history (after reading data from Transactions.log file)");
                            Console.WriteLine("9. Leave from parking");
                            inputKey = Console.ReadLine();
                            Console.Clear();
                            switch (inputKey)
                            {
                                case "1":
                                    try
                                    {
                                        string temp;
                                        Console.WriteLine("All non-parked vehicles\n");
                                        AllVehicles(vehicles);
                                        Console.WriteLine("\nInput id vehicle for parking");
                                        temp = Console.ReadLine();
                                        Vehicle addVehicle = vehicles.Find(x => x.Id == temp);
                                        if(addVehicle != null)
                                        {
                                            parkingService.AddVehicle(addVehicle);
                                            vehicles.Remove(addVehicle);
                                        }
                                        else
                                        {
                                            Console.WriteLine("Vehicle not found");
                                        }
                                    }
                                    catch (ArgumentException e)
                                    {
                                        Console.WriteLine(e);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e);
                                    }
                                    break;
                                case "2":
                                    AllParkedVehicles(parkingService);
                                    string tempRemove;
                                    Console.WriteLine("\nInput id vehicle for parking");
                                    tempRemove = Console.ReadLine();
                                    parkingService.RemoveVehicle(tempRemove);
                                    break;
                                case "3":
                                    AllParkedVehicles(parkingService);
                                    Console.ReadKey();
                                    break;
                                case "4":
                                    Console.WriteLine("Current balance parking - {0}", parkingService.GetBalance());
                                    Console.ReadKey();
                                    break;
                                case "5":
                                    Console.WriteLine("Total Seats - {0}", parkingService.GetCapacity());
                                    Console.WriteLine("Places taken - {0}", parkingService.GetFreePlaces());
                                    Console.ReadKey();
                                    break;
                                case "6":
                                    AllParkedVehicles(parkingService);
                                    Console.WriteLine("\nInput id vehicle");
                                    string upId = Console.ReadLine();
                                    Console.WriteLine("Input recharge amount");
                                    decimal sum = Convert.ToDecimal(Console.ReadLine());
                                    parkingService.TopUpVehicle(upId, sum);
                                    break;
                                case "7":
                                    parkingService.GetLastParkingTransactions();
                                    Console.ReadKey();
                                    break;
                                case "8":
                                    Console.WriteLine(parkingService.ReadFromLog());
                                    Console.ReadKey();
                                    break;
                                case "9":
                                    flagInner = false;
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    case "4":
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("Incorrect input");
                        break;
                }
            }
        }
        static void AllVehicles(List<Vehicle> vehicles)
        {
            foreach (var item in vehicles)
            {
                Console.WriteLine("Id vehicle - {0} ", item.Id);
                Console.WriteLine("Balance - {0} ", item.Balance);
                Console.WriteLine("Type vehicle - {0} ", item.VehicleType);
                Console.WriteLine(new string('-', 40));
            }
        }
        static void AllParkedVehicles(ParkingService parkingService)
        {
            if(parkingService.GetVehicles().Count == 0)
            {
                Console.WriteLine("Parking is empty");
            }
            else
            {
                foreach (var item in parkingService.GetVehicles())
                {
                    Console.WriteLine("Id vehicle - {0} ", item.Id);
                    Console.WriteLine("Balance - {0} ", item.Balance);
                    Console.WriteLine("Type vehicle - {0} ", item.VehicleType);
                    Console.WriteLine(new string('-', 40));
                }
            }
        }
    }
}
