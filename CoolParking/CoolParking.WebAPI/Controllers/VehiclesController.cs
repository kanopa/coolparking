﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpPost]
        public ActionResult AddVehicle([FromBody]Vehicle vehicle)
        {
            try
            {
                parkingService.AddVehicle(vehicle);
                return Created("Car added", vehicle);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
        [HttpGet]
        public ActionResult GetVehicle()
        {
            return Ok(parkingService.GetVehicles());
        }
        [HttpGet("{id}")]
        public ActionResult GetVehicleById(string id)
        {
            try
            {
                return Ok(parkingService.GetVehiclesById(id));
            }
            catch (ArgumentException e)
            {
                return BadRequest("400 Bad Request");
            }
            catch (Exception e)
            {
                return NotFound("404 Not Found");
            }
        }

        [HttpDelete("{id}")]
        public ActionResult RemoveVehicleById(string id)
        {
            try
            {
                parkingService.RemoveVehicle(id);
                return NoContent();
            }

            catch(ArgumentException e)
            {
                return NotFound("400 Not Found");
            }
            catch(Exception e)
            {
                return BadRequest("400 Not Found");
            }
        }
    }
}