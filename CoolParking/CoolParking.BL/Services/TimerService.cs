﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public Timer Timer { get; set; }

        public TimerService()
        {
            Timer = new Timer();
        }

        public void Dispose()
        {
            Timer.Dispose();
        }

        public void Start()
        {
            Timer.Elapsed += Elapsed;
            Timer.Interval = Interval;
            Timer.AutoReset = true;
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Elapsed -= Elapsed;
            Timer.Stop();
        }
    }
}
