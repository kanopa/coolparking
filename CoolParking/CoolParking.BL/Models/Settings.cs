﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Settings
    {
        public Parking Parking { get; set; }
        public int Capacity { get; set; }
        public int Payment { get; set; }
        public int RecordLog {get; set;}
        public decimal RateCar { get; set; }
        public decimal RateTruck { get; set; }
        public decimal RateBus { get; set; }
        public decimal RateMotorcycle { get; set; }

        public decimal Penalty { get; set; }
        public Settings()
        {
            Parking = Parking.getInstance();
            Parking.Balance = 0;
            Capacity = 10;
            Payment = 5000;
            RecordLog = 20000;
            RateCar = 2;
            RateTruck = 5;
            RateBus = 3.5M;
            RateMotorcycle = 1;
            Penalty = 2.5M;
        }
    }
}